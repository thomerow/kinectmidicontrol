﻿namespace Kinect.MIDI {

   using System;
   using System.Collections.Generic;
   using System.ComponentModel;
   using System.Windows;
   using System.Windows.Media;
   using Microsoft.Kinect;
   using MathNet.Numerics.LinearAlgebra;
   using System.Globalization;
   using System.Linq;
   using System.Collections.ObjectModel;

   /// <summary>
   /// Interaction logic for MainWindow
   /// </summary>
   public partial class MainWindow : Window, INotifyPropertyChanged {

      /// <summary>
      /// Radius of drawn hand circles
      /// </summary>
      private const double HandSize = 30;

      /// <summary>
      /// Thickness of drawn joint lines
      /// </summary>
      private const double JointThickness = 3;

      /// <summary>
      /// Thickness of clip edge rectangles
      /// </summary>
      private const double ClipBoundsThickness = 10;

      /// <summary>
      /// Constant for clamping Z values of camera space points from being negative
      /// </summary>
      private const float InferredZPositionClamp = 0.1f;

      /// <summary>
      /// Brush used for drawing hands that are currently tracked as closed
      /// </summary>
      private readonly Brush HandClosedBrush = new SolidColorBrush(Color.FromArgb(128, 255, 0, 0));

      /// <summary>
      /// Brush used for drawing hands that are currently tracked as opened
      /// </summary>
      private readonly Brush HandOpenBrush = new SolidColorBrush(Color.FromArgb(128, 0, 255, 0));

      /// <summary>
      /// Brush used for drawing hands that are currently tracked as in lasso (pointer) position
      /// </summary>
      private readonly Brush HandLassoBrush = new SolidColorBrush(Color.FromArgb(128, 0, 0, 255));

      /// <summary>
      /// Brush used for drawing joints that are currently tracked
      /// </summary>
      private readonly Brush TrackedJointBrush = new SolidColorBrush(Color.FromArgb(255, 68, 192, 68));

      /// <summary>
      /// Brush used for drawing joints that are currently inferred
      /// </summary>        
      private readonly Brush InferredJointBrush = Brushes.Yellow;

      /// <summary>
      /// Pen used for drawing bones that are currently inferred
      /// </summary>        
      private readonly Pen InferredBonePen = new Pen(Brushes.Gray, 1);

      /// <summary>
      /// Drawing group for body rendering output
      /// </summary>
      private DrawingGroup _drawingGroup;

      /// <summary>
      /// Drawing image that we will display
      /// </summary>
      private DrawingImage _imageSource;

      /// <summary>
      /// Active Kinect sensor
      /// </summary>
      private KinectSensor _kinectSensor = null;

      /// <summary>
      /// Coordinate mapper to map one type of point to another
      /// </summary>
      private CoordinateMapper _coordinateMapper = null;

      /// <summary>
      /// Reader for body frames
      /// </summary>
      private BodyFrameReader _bodyFrameReader = null;

      /// <summary>
      /// Array for the bodies
      /// </summary>
      private Body[] _bodies = null;

      /// <summary>
      /// definition of bones
      /// </summary>
      private List<Tuple<JointType, JointType>> _bones;

      /// <summary>
      /// Width of display (depth space)
      /// </summary>
      private int _displayWidth;

      /// <summary>
      /// Height of display (depth space)
      /// </summary>
      private int _displayHeight;

      /// <summary>
      /// List of colors for each body tracked
      /// </summary>
      private Pen[] BodyColors;

      /// <summary>
      /// Current status text to display
      /// </summary>
      private string _statusText = null;

      private Midi.OutputDevice _midiDevice = null;

      /// <summary>
      /// Initializes a new instance of the MainWindow class.
      /// </summary>
      public MainWindow() {
         // one sensor is currently supported
         _kinectSensor = KinectSensor.GetDefault();

         // get the coordinate mapper
         _coordinateMapper = _kinectSensor.CoordinateMapper;

         // get the depth (display) extents
         FrameDescription frameDescription = _kinectSensor.DepthFrameSource.FrameDescription;

         // get size of joint space
         _displayWidth = frameDescription.Width;
         _displayHeight = frameDescription.Height;

         // open the reader for the body frames
         _bodyFrameReader = _kinectSensor.BodyFrameSource.OpenReader();

         // a bone defined as a line between two joints
         _bones = new List<Tuple<JointType, JointType>>();

         // Torso
         _bones.Add(new Tuple<JointType, JointType>(JointType.Head, JointType.Neck));
         _bones.Add(new Tuple<JointType, JointType>(JointType.Neck, JointType.SpineShoulder));
         _bones.Add(new Tuple<JointType, JointType>(JointType.SpineShoulder, JointType.SpineMid));
         _bones.Add(new Tuple<JointType, JointType>(JointType.SpineMid, JointType.SpineBase));
         _bones.Add(new Tuple<JointType, JointType>(JointType.SpineShoulder, JointType.ShoulderRight));
         _bones.Add(new Tuple<JointType, JointType>(JointType.SpineShoulder, JointType.ShoulderLeft));
         _bones.Add(new Tuple<JointType, JointType>(JointType.SpineBase, JointType.HipRight));
         _bones.Add(new Tuple<JointType, JointType>(JointType.SpineBase, JointType.HipLeft));

         // Right Arm
         _bones.Add(new Tuple<JointType, JointType>(JointType.ShoulderRight, JointType.ElbowRight));
         _bones.Add(new Tuple<JointType, JointType>(JointType.ElbowRight, JointType.WristRight));
         _bones.Add(new Tuple<JointType, JointType>(JointType.WristRight, JointType.HandRight));
         _bones.Add(new Tuple<JointType, JointType>(JointType.HandRight, JointType.HandTipRight));
         _bones.Add(new Tuple<JointType, JointType>(JointType.WristRight, JointType.ThumbRight));

         // Left Arm
         _bones.Add(new Tuple<JointType, JointType>(JointType.ShoulderLeft, JointType.ElbowLeft));
         _bones.Add(new Tuple<JointType, JointType>(JointType.ElbowLeft, JointType.WristLeft));
         _bones.Add(new Tuple<JointType, JointType>(JointType.WristLeft, JointType.HandLeft));
         _bones.Add(new Tuple<JointType, JointType>(JointType.HandLeft, JointType.HandTipLeft));
         _bones.Add(new Tuple<JointType, JointType>(JointType.WristLeft, JointType.ThumbLeft));

         // Right Leg
         _bones.Add(new Tuple<JointType, JointType>(JointType.HipRight, JointType.KneeRight));
         _bones.Add(new Tuple<JointType, JointType>(JointType.KneeRight, JointType.AnkleRight));
         _bones.Add(new Tuple<JointType, JointType>(JointType.AnkleRight, JointType.FootRight));

         // Left Leg
         _bones.Add(new Tuple<JointType, JointType>(JointType.HipLeft, JointType.KneeLeft));
         _bones.Add(new Tuple<JointType, JointType>(JointType.KneeLeft, JointType.AnkleLeft));
         _bones.Add(new Tuple<JointType, JointType>(JointType.AnkleLeft, JointType.FootLeft));

         // populate body colors, one for each BodyIndex
         BodyColors = new Pen[] {
            new Pen(Brushes.Red, 6),
            new Pen(Brushes.Orange, 6),
            new Pen(Brushes.Green, 6),
            new Pen(Brushes.Blue, 6),
            new Pen(Brushes.Indigo, 6),
            new Pen(Brushes.Violet, 6)
         };

         // set IsAvailableChanged event notifier
         _kinectSensor.IsAvailableChanged += Sensor_IsAvailableChanged;

         // open the sensor
         _kinectSensor.Open();

         // set the status text
         StatusText = _kinectSensor.IsAvailable ? Properties.Resources.RunningStatusText : Properties.Resources.NoSensorStatusText;

         // Create the drawing group we'll use for drawing
         _drawingGroup = new DrawingGroup();

         // Create an image source that we can use in our image control
         _imageSource = new DrawingImage(_drawingGroup);

         // use the window object as the view model in this simple example
         DataContext = this;

         // initialize the components (controls) of the window
         InitializeComponent();
      }

      /// <summary>
      /// INotifyPropertyChangedPropertyChanged event to allow window controls to bind to changeable data
      /// </summary>
      public event PropertyChangedEventHandler PropertyChanged;

      /// <summary>
      /// Gets the bitmap to display
      /// </summary>
      public ImageSource ImageSource => _imageSource;

      /// <summary>
      /// Gets or sets the current status text to display
      /// </summary>
      public string StatusText {
         get {
            return _statusText;
         }

         set {
            if (_statusText != value) {
               _statusText = value;

               // notify any bound elements that the text has changed
               PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("StatusText"));
            }
         }
      }

      /// <summary>
      /// Execute start up tasks
      /// </summary>
      /// <param name="sender">object sending the event</param>
      /// <param name="e">event arguments</param>
      private void MainWindow_Loaded(object sender, RoutedEventArgs e) {
         if (_bodyFrameReader != null) {
            _bodyFrameReader.FrameArrived += Reader_FrameArrived;
         }
      }

      /// <summary>
      /// Execute shutdown tasks
      /// </summary>
      /// <param name="sender">object sending the event</param>
      /// <param name="e">event arguments</param>
      private void MainWindow_Closing(object sender, CancelEventArgs e) {
         if (_bodyFrameReader != null) {
            // BodyFrameReader is IDisposable
            _bodyFrameReader.Dispose();
            _bodyFrameReader = null;
         }

         if (_kinectSensor != null) {
            _kinectSensor.Close();
            _kinectSensor = null;
         }
      }

      /// <summary>
      /// Handles the body frame data arriving from the sensor
      /// </summary>
      /// <param name="sender">object sending the event</param>
      /// <param name="e">event arguments</param>
      private void Reader_FrameArrived(object sender, BodyFrameArrivedEventArgs e) {
         bool dataReceived = false;

         using (BodyFrame bodyFrame = e.FrameReference.AcquireFrame()) {
            if (bodyFrame != null) {
               if (_bodies == null) {
                  _bodies = new Body[bodyFrame.BodyCount];
               }

               // The first time GetAndRefreshBodyData is called, Kinect will allocate each Body in the array.
               // As long as those body objects are not disposed and not set to null in the array,
               // those body objects will be re-used.
               bodyFrame.GetAndRefreshBodyData(_bodies);
               dataReceived = true;
            }
         }

         if (dataReceived) {
            using (DrawingContext dc = _drawingGroup.Open()) {
               // Draw a transparent background to set the render size
               dc.DrawRectangle(Brushes.Black, null, new Rect(0.0, 0.0, _displayWidth, _displayHeight));

               int penIndex = 0;
               foreach (Body body in _bodies) {
                  Pen drawPen = BodyColors[penIndex++];

                  if (body.IsTracked) {
                     DrawClippedEdges(body, dc);

                     IReadOnlyDictionary<JointType, Joint> joints = body.Joints;

                     // convert the joint points to depth (display) space
                     Dictionary<JointType, Point> jointPoints = new Dictionary<JointType, Point>();

                     foreach (JointType jointType in joints.Keys) {
                        // sometimes the depth(Z) of an inferred joint may show as negative
                        // clamp down to 0.1f to prevent coordinatemapper from returning (-Infinity, -Infinity)
                        CameraSpacePoint position = joints[jointType].Position;
                        if (position.Z < 0) {
                           position.Z = InferredZPositionClamp;
                        }

                        DepthSpacePoint depthSpacePoint = _coordinateMapper.MapCameraPointToDepthSpace(position);
                        jointPoints[jointType] = new Point(depthSpacePoint.X, depthSpacePoint.Y);
                     }

                     DrawBody(joints, jointPoints, dc, drawPen);

                     DrawHand(body.HandLeftState, jointPoints[JointType.HandLeft], dc);
                     DrawHand(body.HandRightState, jointPoints[JointType.HandRight], dc);

                     // Line through shoulders
                     // DisplayDistanceBetweenPointAndLine(joints, jointPoints, JointType.HandRight, JointType.ShoulderRight, JointType.ShoulderLeft, JointType.HandRight, dc);

                     // Line through spine base and spine shoulder
                     DisplayDistanceBetweenPointAndLine(joints, jointPoints, JointType.HandRight, JointType.SpineBase, JointType.SpineShoulder, JointType.HandRight, dc);
                  }
               }

               // prevent drawing outside of our render area
               _drawingGroup.ClipGeometry = new RectangleGeometry(new Rect(0.0, 0.0, _displayWidth, _displayHeight));
            }
         }
      }

      private void DisplayDistanceBetweenPointAndLine(
         IReadOnlyDictionary<JointType, Joint> joints, 
         Dictionary<JointType, Point> jointPoints, 
         JointType followedJoint, 
         JointType jointLine0, 
         JointType jointLine1, 
         JointType jointTextPos, 
         DrawingContext dc) {

         CameraSpacePoint point = joints[followedJoint].Position;
         CameraSpacePoint pointLine0 = joints[jointLine0].Position;
         CameraSpacePoint pointLine1 = joints[jointLine1].Position;
         Point pointTextPos = jointPoints[jointTextPos];

         var u = Vector<double>.Build.Dense(new double[] { pointLine1.X - pointLine0.X, pointLine1.Y - pointLine0.Y, pointLine1.Z - pointLine0.Z });
         var pq = Vector<double>.Build.Dense(new double[] { point.X - pointLine0.X, point.Y - pointLine0.Y, point.Z - pointLine0.Z });
         double distance = pq.CrossProduct(u).L2Norm() / u.L2Norm();

         // Highlight involved joints
         Point p2DLine0 = jointPoints[jointLine0];
         Point p2DLine1 = jointPoints[jointLine1];
         var white = new SolidColorBrush(Colors.White);
         dc.DrawEllipse(white, null, pointTextPos, 5, 5);
         dc.DrawEllipse(white, null, p2DLine0, 5, 5);
         dc.DrawEllipse(white, null, p2DLine1, 5, 5);

         pointTextPos.Y -= 20;
         dc.DrawText(
            new FormattedText(distance.ToString("0.00m", CultureInfo.InvariantCulture), CultureInfo.InvariantCulture, 
            FlowDirection.LeftToRight, new Typeface("Consolas"), 16, new SolidColorBrush(Colors.White)), pointTextPos);
      }


      /// <summary>
      /// Draws a body
      /// </summary>
      /// <param name="joints">joints to draw</param>
      /// <param name="jointPoints">translated positions of joints to draw</param>
      /// <param name="drawingContext">drawing context to draw to</param>
      /// <param name="drawingPen">specifies color to draw a specific body</param>
      private void DrawBody(IReadOnlyDictionary<JointType, Joint> joints, IDictionary<JointType, Point> jointPoints, DrawingContext drawingContext, Pen drawingPen) {
         // Draw the bones
         foreach (var bone in _bones) {
            DrawBone(joints, jointPoints, bone.Item1, bone.Item2, drawingContext, drawingPen);
         }

         // Draw the joints
         foreach (JointType jointType in joints.Keys) {
            Brush drawBrush = null;

            TrackingState trackingState = joints[jointType].TrackingState;

            if (trackingState == TrackingState.Tracked) {
               drawBrush = TrackedJointBrush;
            }
            else if (trackingState == TrackingState.Inferred) {
               drawBrush = InferredJointBrush;
            }

            if (drawBrush != null) {
               drawingContext.DrawEllipse(drawBrush, null, jointPoints[jointType], JointThickness, JointThickness);
            }
         }
      }

      /// <summary>
      /// Draws one bone of a body (joint to joint)
      /// </summary>
      /// <param name="joints">joints to draw</param>
      /// <param name="jointPoints">translated positions of joints to draw</param>
      /// <param name="jointType0">first joint of bone to draw</param>
      /// <param name="jointType1">second joint of bone to draw</param>
      /// <param name="drawingContext">drawing context to draw to</param>
      /// /// <param name="drawingPen">specifies color to draw a specific bone</param>
      private void DrawBone(IReadOnlyDictionary<JointType, Joint> joints, IDictionary<JointType, Point> jointPoints, JointType jointType0, JointType jointType1, DrawingContext drawingContext, Pen drawingPen) {
         Joint joint0 = joints[jointType0];
         Joint joint1 = joints[jointType1];

         // If we can't find either of these joints, exit
         if (joint0.TrackingState == TrackingState.NotTracked ||
             joint1.TrackingState == TrackingState.NotTracked) {
            return;
         }

         // We assume all drawn bones are inferred unless BOTH joints are tracked
         Pen drawPen = InferredBonePen;
         if ((joint0.TrackingState == TrackingState.Tracked) && (joint1.TrackingState == TrackingState.Tracked)) {
            drawPen = drawingPen;
         }

         drawingContext.DrawLine(drawPen, jointPoints[jointType0], jointPoints[jointType1]);
      }

      /// <summary>
      /// Draws a hand symbol if the hand is tracked: red circle = closed, green circle = opened; blue circle = lasso
      /// </summary>
      /// <param name="handState">state of the hand</param>
      /// <param name="handPosition">position of the hand</param>
      /// <param name="drawingContext">drawing context to draw to</param>
      private void DrawHand(HandState handState, Point handPosition, DrawingContext drawingContext) {
         switch (handState) {
            case HandState.Closed:
               drawingContext.DrawEllipse(HandClosedBrush, null, handPosition, HandSize, HandSize);
               break;

            case HandState.Open:
               drawingContext.DrawEllipse(HandOpenBrush, null, handPosition, HandSize, HandSize);
               break;

            case HandState.Lasso:
               drawingContext.DrawEllipse(HandLassoBrush, null, handPosition, HandSize, HandSize);
               break;
         }
      }

      /// <summary>
      /// Draws indicators to show which edges are clipping body data
      /// </summary>
      /// <param name="body">body to draw clipping information for</param>
      /// <param name="drawingContext">drawing context to draw to</param>
      private void DrawClippedEdges(Body body, DrawingContext drawingContext) {
         FrameEdges clippedEdges = body.ClippedEdges;

         if (clippedEdges.HasFlag(FrameEdges.Bottom)) {
            drawingContext.DrawRectangle(
                Brushes.Red,
                null,
                new Rect(0, _displayHeight - ClipBoundsThickness, _displayWidth, ClipBoundsThickness));
         }

         if (clippedEdges.HasFlag(FrameEdges.Top)) {
            drawingContext.DrawRectangle(
                Brushes.Red,
                null,
                new Rect(0, 0, _displayWidth, ClipBoundsThickness));
         }

         if (clippedEdges.HasFlag(FrameEdges.Left)) {
            drawingContext.DrawRectangle(
                Brushes.Red,
                null,
                new Rect(0, 0, ClipBoundsThickness, _displayHeight));
         }

         if (clippedEdges.HasFlag(FrameEdges.Right)) {
            drawingContext.DrawRectangle(
                Brushes.Red,
                null,
                new Rect(_displayWidth - ClipBoundsThickness, 0, ClipBoundsThickness, _displayHeight));
         }
      }

      /// <summary>
      /// Handles the event which the sensor becomes unavailable (E.g. paused, closed, unplugged).
      /// </summary>
      /// <param name="sender">object sending the event</param>
      /// <param name="e">event arguments</param>
      private void Sensor_IsAvailableChanged(object sender, IsAvailableChangedEventArgs e) {
         // on failure, set the status text
         StatusText = _kinectSensor.IsAvailable ? Properties.Resources.RunningStatusText : Properties.Resources.SensorNotAvailableStatusText;
      }

      private void BtnSelMidiDev_Click(object sender, RoutedEventArgs e) {
         var devNames = from dev in Midi.OutputDevice.InstalledDevices
                        where !dev.IsOpen select dev.Name;
         var dlg = new MidiDeviceSelectionDialog { Owner = this };
         dlg.DeviceNames = new ObservableCollection<string>(devNames.ToList());
         if (dlg.ShowDialog() == true) {

            // ToDo: implement

         }
      }
   }

   internal static partial class Extensions {
      public static Vector<double> CrossProduct(this Vector<double> vector, Vector<double> other) {
         if (vector.Count != 3 || other.Count != 3) throw new ArgumentException("Vectors must have a length of 3.");
         var result = Vector<double>.Build.Dense(3);
         result[0] = vector[1] * other[2] - vector[2] * other[1];
         result[1] = vector[0] * other[2] + vector[2] * other[0];
         result[2] = vector[0] * other[1] - vector[1] * other[0];
         return result;
      }
   }
}
