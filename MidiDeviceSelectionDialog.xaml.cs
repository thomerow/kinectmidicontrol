﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Kinect.MIDI {

   /// <summary>
   /// Interaction logic for MidiDeviceSelectionWindow.xaml
   /// </summary>
   public partial class MidiDeviceSelectionDialog : Window {

      public MidiDeviceSelectionDialog() {
         InitializeComponent();
         LocalizeDialog();

         // use the window object as the view model
         DataContext = this;
         DeviceNames = new ObservableCollection<string>();
      }

      private void LocalizeDialog() {
         Title = Properties.Resources.MidiDevSelWnd_Title;
         _btnCancel.Content = Properties.Resources.BtnCancelText;
         _btnOK.Content = Properties.Resources.BtnOKText;
      }

      public ObservableCollection<string> DeviceNames { get; set; }

      private void BtnOK_Click(object sender, RoutedEventArgs e) {
         DialogResult = true;
         Close();
      }
   }
}
